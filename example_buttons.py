import simplified_pygame


WINDOW = simplified_pygame.PyGameWindow(640, 480, bg_color=(100, 100, 100))


class AppControlls(simplified_pygame.EventReaderAsClass):

    def on_key_f1():
        WINDOW.set_window_resolution(1)
    def on_key_f2():
        WINDOW.set_window_resolution(2)
    def on_key_f3():
        WINDOW.set_window_resolution('fullscreen')
    def on_key_escape():
        WINDOW.on_exit()


TEXT = ''

class Buttons(simplified_pygame.EventReader):

    buttons = [(200, 100, 'A'), (200, 300, 'B'), (400, 100, 'C'), (400, 300, 'D')]

    def __init__(self):
        self.last_pressed = None

    def mouse_map(self, x, y):
        for a, b, key in self.buttons:
            if a <= x <= a+100 and b <= y <= b + 100:
                return key
        return None

    def on_mouse_move(self, key):
        # pos could be 'A', 'B', 'C', 'D' or None
        global TEXT
        TEXT += f'button moved over: {key}\n'
        self.start_timer('clear_text', 1000, reset=False)

    def on_mouse_click(self, key):
        # pos could be 'A', 'B', 'C' or 'D'
        global TEXT
        # using delayed_setattr, light up the button for one second
        self.last_pressed = key
        self.delayed_setattr('last_pressed', None, 1000)

        # using timers, change the text for two seconds
        TEXT += f'button clicked: {key}\n'
        self.start_timer('clear_text', 2000, reset=False)

    def on_timer_clear_text(self):
        global TEXT
        _, TEXT = TEXT.split('\n', 1)

    def draw(self):
        for x, y, key in self.buttons:
            if self.last_pressed == key:
                WINDOW.rect((x, y, 100, 100), col=(255, 255, 255))
                WINDOW.write(x+50, y+30, key, font=None, size=40, pos='.', col=(0, 0, 0))
            # ._mouse_pos would be set automatically, as mouse_map is declated
            # it is equal to the results of the last call of mouse_map()
            elif self._mouse_pos == key:
                WINDOW.rect((x, y, 100, 100), col=(150, 150, 150))
                WINDOW.write(x+50, y+30, key, font=None, size=40, pos='.', col=(0, 0, 0))
            else:
                WINDOW.box((x, y, 100, 100), col=(150, 150, 150))
                WINDOW.write(x+50, y+30, key, font=None, size=40, pos='.', col=(200, 200, 200))


buttons = Buttons()


for events, time_passed, key_pressed in WINDOW.main_loop(framerate=10):
    AppControlls.read_events(events, time_passed, key_pressed)
    buttons.read_events(events, time_passed, key_pressed)
    buttons.draw()
    WINDOW.write(10, 10, TEXT, font=None, size=20, col=(200, 230, 200))

