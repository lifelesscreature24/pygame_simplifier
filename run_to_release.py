"""
Template code.
Fill the blanks fields and run to make the python file into executable using cx_Freeze.
Executable is save and archived into the "/build" subfolder.
"""

import sys
import os
import shutil
import platform
from cx_Freeze import setup, Executable


# blanks:
AUTHOR = 'your name here'
VERSION = '1.0'
NAME = 'my_game'
DESCRIPTION = 'My Game'
OUTPUT_FOLDER = 'my_game'
TARGET_FILE = 'main.py'


if __name__ == '__main__':
    os_family = {'Windows': 'win', 'Darwin': 'mac', 'Linux': 'linux'}[platform.system()]
    sys.argv[:] = [sys.argv[0], 'build']

    buildOptions = dict(
        packages=[],
        excludes=[],
        include_files=[('assets', 'assets')])

    target = Executable(
        TARGET_FILE,
        base={'win': 'Win32GUI', 'mac': None, 'linux': None}[os_family],
        targetName='game',
        icon='assets/icon.' + {'win': 'ico', 'mac': 'icns', 'linux': 'ico'}[os_family],
        copyright=AUTHOR,
    )

    setup(
        name=NAME,
        version=VERSION,
        description=DESCRIPTION,
        options={'build_exe': buildOptions},
        executables=[target]
        )

    # zip files
    build_dir = 'build/'+os.listdir('build')[-1]
    print(f'build/{OUTPUT_FOLDER}_{os_family}_v{VERSION}')
    shutil.make_archive(f'build/{OUTPUT_FOLDER}_{os_family}_v{VERSION}', 'zip', build_dir)
